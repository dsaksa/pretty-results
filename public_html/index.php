 <!DOCTYPE html>
 <html>
 <head>
   <title>Test</title>
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
 </head>

 <link rel="stylesheet" type="text/css" href="style.css">
 <body>
<pre>
<?php 
  class EventParser {
    public $events = [];
    private $header = "";
    private $builtRegex = "";
    private $columnHeaders = [];
    private $parsedResults = [];
    

    public function __construct($file) {
      $result = preg_split('/$\R?^/m', $file);
      $this -> runLoop($result);
      // print_r($this -> events);
    }

    private function runLoop($file) {
      $eventCount = 0;
      $equalsCounter = 0;
      $inEvent = false;
      $placeCount = 0;
      // Loop through string
      for($i = 0; $i < count($file); $i++) {
        // If ======
        if(preg_match_all('/=+/', $file[$i])) {

          // If not in event already, this is the one below the title 
          if($equalsCounter == 0) {
            $eventTitle = $file[$i - 1];
            $eventType = $this->determineType($file[$i - 1]);
            $placeCount = 0;

            if($eventType != 'ranking') {
              $inEvent = false;
              $eventCount = $eventCount + 1;
              $this->createEventEntry($eventCount, $eventType, $eventTitle);
            } else {
              continue;
            }

            $equalsCounter = $equalsCounter + 1;
          } else {
            $this->events[$eventCount]['headers'] = $this->parse_column_headers($eventCount, $file[$i - 1]);
            $this->events[$eventCount]['regex'] = $this->build_regex($this->events[$eventCount]['headers']);
            $this->events[$eventCount]['finals'] = $this->isFinals($file[$i + 1]);
            // Search for event notes here
            $equalsCounter = 0;
            $inEvent = true;
          }
        } else {
          $wasValidResult = $this->parse_result_line($eventCount, $file[$i], $inEvent, $placeCount);

          if($wasValidResult) {
            $placeCount = $placeCount + 1;
            // var_dump('increment $placeCount: '.$placeCount);
          }
        }
      }
    }

    private $searches = [
      "Place"  => [
        "regex" => "^\s{1,3}(?'Place'\d{1,3}|[\-]+)",
        "type" => "digit",
        "optional" => false,
      ],
      "Athlete"   => [
        "regex" => "(?'Athlete'[^\d]{1,24})",
        "type" => "text",
        "optional" => false,
      ],
      "Name"   => [
        "regex" => "(?'Name'[^\d]{1,24})",
        "type" => "text",
        "optional" => false,
      ],
      "Year"   => [
        "regex" => "(?'Year'[\d]+|[FJSOR]+|)",
        "type" => "digit",
        "optional" => false,
      ],
      "Age"   => [
        "regex" => "(?'Age'[\d]+)",
        "type" => "digit",
        "optional" => false,
      ],
      "School" => [
        "regex" => "(?'School'[^\d]{1,20})",
        "type" => "text",
        "optional" => false,
      ],
      "Team" => [
        "regex" => "(?'Team'[^\d]{1,20})",
        "type" => "text",
        "optional" => false,
      ],
      "Prelims" => [
        "regex" => "(?'Prelims'[\d:.]+|[\d:.q]+|[DQFSN]+|[* ]{1,}[\d:.q]+)",
        "type" => "digit",
        "optional" => false,
      ],
      "Seed"   => [
        "regex" => "\s+(?'Seed'[\d:.]+|[\d:.q*]+|[\d-.J*]+|\s{2,10})",
        "type" => "digit",
        "optional" => false,
      ],
      "Finals" => [
        "regex" => "(?'Finals'[\d:.]+|[\d.:qJ#*]+|[\d-.J*]+|[DNSCRFSQNHOUL]+|[x ]{1,}[\d.:]+|\s)",
        "type" => "digit",
        "optional" => true,
      ],
      "Wind" => [
        "regex" => "(?(?=[-]|[\d]|[NWI])(?'Wind'[-\d\.]{3,4}|[NWI]+)|)",
        "type" => "mixed",
        "optional" => true
      ],
      "H#"     => [
        "regex" => "(?'Heat'\d{0,2}|\s)",
        "type" => "digit",
        "optional" => true,
      ],
      "Points" => [
        "regex" => "(?(?=\d)(?'Points'\d{0,2}))",
        "type" => "digit",
        "optional" => true,
      ],
      "Note" => [
        "regex" => "(?(?!\s+$)(?'Notes'.*)|\s)",
        "type" => "text",
        "optional" => true,
      ]
    ];

    private $utility = [
      "optional" => "(?(?![\s]+\\n|$)\s+|.*$)"
    ];

    private function createEventEntry($id, $type, $title) {
      $this->events[$id] = [
        'data' => [],
        'finals' => null,
        'headers' => '',
        'regex' => '',
        'title' => $title,
        'type' => $type,
        'extra' => [],
      ];
      // final, prelim, relay, decathalon,
    }

    private function isFinals($line) {
      // var_dump($line);
      if(preg_match('/Preliminaries/i', $line)) {
        return false;
      } else {
        return true;
      }
    }

    private function determineType($string) {
      if(preg_match('/relay|medley/i', $string)) {
        return 'relay';
      } else if (preg_match('/Put|Vault|Throw/i', $string)) {
        return 'field';
      } else if (preg_match('/Jump/i', $string)) {
        return 'jump';
      } else if (preg_match('/Rankings/i', $string)) {
        return 'ranking';
      } else {
        return 'single';
      }
    }

    private function parse_result_line($ID, $line, $inEvent, $placeCount) {
      // var_dump($ID);
      // var_dump($line);
      // var_dump($inEvent);
      if(preg_match('/^\s{1,3}(\d{1,3}|[\-]+)(?=\s)/m', $line)) {
        $pattern = '/'.$this->events[$ID]['regex'].'/';
        preg_match_all($pattern, $line, $matches);

        if($matches) {
          $this->events[$ID]['data'] = array_merge($this->events[$ID]['data'], [$matches]);
        } else {
          var_dump($line);
          print('error with above line');
        }

        return true;
      } else if($inEvent && !preg_match('/Preliminaries|Finals|^[\s]*$|^[^\s].*$/', $line)) {
        // var_dump($line);
        if(!array_key_exists($placeCount, $this->events[$ID]['extra'])) {
          // var_dump($placeCount);
          $this->events[$ID]['extra'][$placeCount] = Array();
          // var_dump($this->events[$ID]['extra']);
        // } else {
          // var_dump('should be an array');
          // var_dump($this->events[$ID]['extra'][0]);
        }

        // var_dump($this->events[$ID]['extra'][$placeCount]);

        $this->events[$ID]['extra'][$placeCount] = array_merge($this->events[$ID]['extra'][$placeCount], [trim($line)]);
        // print_r($this->events[$ID]['extra'][$placeCount]);

        return false;
      } else {
        // var_dump('Couldn\'t match: '. $line);
        return false;
      }

      return false;
    }

    private function parse_column_headers($id, $raw) {
      preg_match_all("/[^\s]+/", $raw, $columnHeaders);
      return $columnHeaders[0];
    }

    private function build_regex($headers) {
      $built = "";
      // var_dump($this->searches);

      for($i = 0; count($headers) > $i; $i++) {
        $search = $this->searches[$headers[$i]];

        if(is_null($search)) {
          $this->throwError('error searching for ', $headers[$i]);
        }

        if($built == "") {
          $built .= $this->searches['Place']['regex'];
        }

        if($search['optional'] == true) {
          $built .= $this->utility['optional'];
        } else if($headers[$i] == 'Seed') {
          // Do nothing
        } else {
          $built .= "\s+";
        }

        if($search['type'] == 'text') {
          $built .= $search['regex'];
        } else {
          if($search['type'] == 'digit' && $this->searches[$headers[$i-1]]['type'] == 'digit' && !$this->searches[$headers[$i-1]]['optional']) {
            $built .= $search['regex'];
          } else {
            $built .= $search['regex'];
          }
        }
      }

      // Add the note catcher at the end
      $built.= $this->searches['Note']['regex'];

      // var_dump($built);

      return $built;
    }

    private function throwError($text, $variable) {
      echo($text.': '.$variable.'<br />');
      // print_r($this->events);
    }
  }
?>



<?php 
  $file = file_get_contents('test.html');
  $eventParser = new EventParser($file);
  $results = $eventParser->events;
  // print_r($results);
  for($i = 1; count($results) > $i; $i++):
?>
</pre>
<h1><?php echo $results[$i]['title']; if(!$results[$i]['finals']){echo (" (Prelims)");}; ?></h1>
<pre><?php echo $results[$i]['type']; ?></pre>
<pre><?php echo $results[$i]['regex']; ?></pre>

<table class="Results">
<?php if ($results[$i]['type'] == 'single'): ?>
  <tr class="Results__header">
    <?php foreach ($results[$i]['headers'] as $key => $header): ?>
      <?php if($key == 0): ?>
        <th>Place</th>
      <?php endif; ?>

      <th><?php echo($header); ?></th>

    <?php endforeach; ?>
   </tr>
    
    <?php foreach($results[$i]['data'] as $line): ?>
      <?php $usedSpecial = false; ?>

      <?php if($line['Place'][0] == "--" || $line['Place'][0] == "-"): ?>
        <tr class="Results__row Results__row--disqualified">
      <?php else: ?>
        <tr class="Results__row">
      <?php endif; ?>
      
      <?php foreach ($line as $key => $value): ?>
        <?php if(!is_numeric($key)): ?>
          <?php if($key == 'Place'): ?>
            <td class="Results__item Results__item--place">
          <?php elseif(($key == 'Name' || $key == 'Team' || $key == 'School') && !$usedSpecial): ?>
            <td class="Results__item  Results__item--special">
            <?php $usedSpecial = true; ?>
          <?php elseif(($key != "School" && $key != "Group" && $key != "Year") && $usedSpecial): ?>
            <td class="Results__item">
          <?php endif; ?>

            <?php if($key == 'Name' || $key == 'Team' || ($key == 'School' && $results[$i]['type'] == 'final')): ?>
                <div class="Results__item-name">
                 <?php echo($value[0]); ?>
                </div>
                <div class="Results__item-year">
                  <?php if(preg_match('/^[^\d]/',$line['Year'][0])): ?>
                    <?php echo($line['Year'][0]); ?>
                  <?php else: ?>
                    '<?php if(strlen($line['Year'][0]) == 1){echo "0";} ?><?php echo($line['Year'][0]); ?>
                  <?php endif ?>
                </div>
                <div class="Results__item-team">
                  <?php echo($line['School'][0]); ?> 
                </div>

            <?php else: ?>

              <?php if ($key != "School" && $key != "Group" && $key != "Year"): ?>
               <?php echo($value[0]); ?>
             <?php endif; ?>

            <?php endif; ?>

          </td>
        <?php endif; ?> 
      <?php endforeach ?>
    </tr>
    <?php endforeach; ?>

<?php else: ?>
  <tr class="Results__header">
    <?php foreach ($results[$i]['headers'] as $key => $header): ?>
      <?php if($key == 0): ?>
        <th>Place</th>
      <?php endif; ?>
      <th><?php echo($header); ?></th>
    <?php endforeach; ?>
   </tr>
    
   <?php $outputTick = 1; ?>
    <?php foreach($results[$i]['data'] as $line): ?>
      <?php $usedSpecial = false; ?>

    <tr class="Results__row">
      <?php foreach ($line as $key => $value): ?>
        <?php if(!is_numeric($key)): ?>

          <?php if($key == 'Place'): ?>
            <td class="Results__item Results__item--place">
          <?php elseif(($key == 'Name' || $key == 'Team' || $key == 'School') && !$usedSpecial): ?>
            <td class="Results__item  Results__item--special">
            <?php $usedSpecial = true; ?>
          <?php else: ?>
            <td class="Results__item">
          <?php endif; ?>

          <?php if(($key == 'Name' || $key == 'Team' || ($key == 'School') && $results[$i]['type'] == 'relay') || ($key == 'Name' && $results[$i]['type'] == 'field')): ?>
            <div class="Results__item-name">
             <?php echo($value[0]); ?>
            </div>
            <div class="Results__item-extra">
              <?php if(array_key_exists($outputTick, $results[$i]['extra'])): ?>
                <?php foreach ($results[$i]['extra'][$outputTick] as $key => $extraLine): ?>
                  <span class="Results__item-extra-line"><?php echo $extraLine; ?></span> <br />
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          <?php else: ?>
            <?php echo($value[0]); ?>
          <?php endif; ?>
        </td>
      <?php endif; ?> 
    <?php endforeach ?>
      <?php $outputTick = $outputTick + 1; ?>
    </tr>

    <?php endforeach ?>
<?php endif; ?>
</table>
<?php endfor; ?>
 </body>
 </html>